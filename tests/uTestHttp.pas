(**
 * This is a part of Delphi+TDD course
 * (c) 2013 contact@frantic.im
 *)

unit uTestHttp;

interface

uses
  TestFramework;

type
  THttpTestCase = class(TTestCase)
  published
    procedure TestTruth;
  end;

implementation

procedure THttpTestCase.TestTruth;
begin
  Check(True, 'My argument is supposed to be True');
end;

initialization
  RegisterTest(THttpTestCase.Suite);

end.
