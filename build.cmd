@echo off

rem TODO #1: Check if you have .NET Framework and the path to MSBuild is correct
set msb=c:\Windows\Microsoft.NET\Framework\v2.0.50727\MSBuild.exe

rem TODO #2: Following path depends on your RAD Studio version
set path=C:\Program Files\Embarcadero\RAD Studio\7.0\bin;%path%
call rsvars.bat

echo Building tests...
del %~dp0bin\WeatherInfoTests.exe 2> NUL
%msb% %~dp0delphi\WeatherInfoTests.dproj /p:DCC_Define="CONSOLE_TESTRUNNER" > %~dp0bin\last-build-log.txt

if not exist %~dp0bin\WeatherInfoTests.exe goto buildfailed

cls
%~dp0bin\WeatherInfoTests.exe
if %errorlevel% neq 0 goto testsfailed
goto testspassed

:buildfailed
color 0c
echo Build failed. Check error log in bin\last-build-log.txt
%~dp0bin\last-build-log.txt
goto done

:testsfailed
color 0c
echo Tests failed
goto done

:testspassed
color 0a
goto done

:done
pause
color 0f
